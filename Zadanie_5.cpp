﻿#include <GL/glut.h>
#include <vector>
#include <string>

struct Point
{
	GLint x;
	GLint y;
};

std::vector<Point> vertices;
int menuHandle;

void RenderScene()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor3f(1.0, 0.0, 0.0);

	glBegin(GL_POLYGON);
	for (const Point& vertex : vertices)
		glVertex2i(vertex.x, vertex.y);
	glEnd();

	glPointSize(3.0);
	glBegin(GL_POINTS);
	for (const Point& vertex : vertices)
		glVertex2i(vertex.x, vertex.y);
	glEnd();

	glutSwapBuffers();
}

void ChangeSize(GLsizei w, GLsizei h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	RenderScene();
}

void ContextMenu(int value)
{

}

void UpdateMenu()
{
	glutCreateMenu(ContextMenu);
	glutAddMenuEntry(("Ilosc wierzcholkow: " + std::to_string(vertices.size())).c_str(), 0);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void MouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		vertices.push_back({ x, glutGet(GLUT_WINDOW_HEIGHT) - y });

		UpdateMenu();
	}
}

void main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Wielokat");
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ChangeSize);
	glutMouseFunc(MouseButton);

	UpdateMenu();

	glutMainLoop();
}